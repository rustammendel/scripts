Some bash scripts and functions
Add this to .bashrc: 

#Scripts folder
PATH=$PATH:~/scripts
export PATH

#bash aliases in scripts folder
if [ -f ~/scripts/.bash_aliases ]; then
    . ~/scripts/.bash_aliases
fi
 

#change "scripts" with the directory path
