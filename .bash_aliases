#Clean Functions
. ~/scripts/clean


#Git aliases
alias ga='git add'
alias gp='git push'
alias gst='git status'
alias gcm='git commit -m '
alias glog='git log --oneline --decorate --graph --all'

#Debian aliases
alias aptrun='sudo apt update &&sudo apt upgrade -y &&\
sudo apt autoremove &&sudo apt autoclean'